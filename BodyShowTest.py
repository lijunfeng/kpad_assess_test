#!/usr/bin/env python
#-*- coding=UTF-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0

# select b.* from member a inner join respondent b on a.id =b.member_id where a.id =25 order by create_time; 

from time import sleep
class main_test(object):
    '''
    test
    '''

    def log_in(self,url,user,password):
        '''
        pass and finish.
        '''

        self.driver = webdriver.Chrome()
        self.driver.get(url)
        self.driver.find_element_by_name('email').send_keys(user)
        self.driver.find_element_by_name('passwd').send_keys(password)
        self.driver.find_element_by_class_name('login_btn').submit()

    def login_client (self,name,sex,height,weight,b1,b2,b3,phone,email,diastolic,systolic,heart,gestation='0',lactation='0'):
        '''
        test1
        no pass and finish
        '''
        if sex =='man':
            self.driver.find_element_by_name('un').send_keys(name)
            self.driver.find_element_by_xpath("//dd/input[1]").click()
            self.driver.find_element_by_name('he').send_keys(height)
            self.driver.find_element_by_name('we').send_keys(weight)
            self.driver.find_element_by_name('ye').send_keys(b1)
            self.driver.find_element_by_name('mo').send_keys(b2)
            self.driver.find_element_by_name('da').send_keys(b3)
            self.driver.find_element_by_name('te').send_keys(phone)
            self.driver.find_element_by_name('em').send_keys(email)
            self.driver.find_element_by_name('sz').send_keys(diastolic)
            self.driver.find_element_by_name('ss').send_keys(systolic)
            self.driver.find_element_by_name('xl').send_keys(heart)
        elif sex =='woman':
            self.driver.find_element_by_name('un').send_keys(name)
            self.driver.find_element_by_xpath("//dd/input[2]").click()
            self.driver.find_element_by_name('he').send_keys(height)
            self.driver.find_element_by_name('we').send_keys(weight)
            self.driver.find_element_by_name('ye').send_keys(b1)
            self.driver.find_element_by_name('mo').send_keys(b2)
            self.driver.find_element_by_name('da').send_keys(b3)
            self.driver.find_element_by_name('te').send_keys(phone)
            self.driver.find_element_by_name('em').send_keys(email)
            self.driver.find_element_by_name('sz').send_keys(diastolic)
            self.driver.find_element_by_name('ss').send_keys(systolic)
            self.driver.find_element_by_name('xl').send_keys(heart)

            g = self.driver.find_element_by_name('yq')
            Select(g).select_by_value(gestation)

            l = self.driver.find_element_by_name('br')
            Select(l).select_by_value(lactation)
        else:
            print 'Erro please select man or woman'

        self.driver.find_element_by_class_name('start_test').submit()
        self.driver.find_element_by_xpath("//div[contains(@class,'l_foot')]/a").click()


    def lactationweek1to6(self,height,pre_pregnancy_weight,cur_weight,his_pressure1,his_pressure2,cur_pressure1,cur_pressure2):
        self.driver.find_element_by_xpath("//div[contains(@id,'main')]/div[2]/div/a").click()
        #1
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.ID,'body_height')))
        element.send_keys(height)
        self.driver.find_element_by_id('prepregnancy_weight').send_keys(pre_pregnancy_weight)
        self.driver.find_element_by_id('current_weight').send_keys(cur_weight)
        #self.driver.find_element_by_id('green_btn').click()
        self.driver.find_element_by_class_name('goto_next').click()
        #2
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.ID,'history_pressure1')))
        element.send_keys(his_pressure1)
        self.driver.find_element_by_id('history_pressure2').send_keys(his_pressure2)
        self.driver.find_element_by_id('current_pressure1').send_keys(cur_pressure1)
        self.driver.find_element_by_id('current_pressure2').send_keys(cur_pressure2)
        #self.driver.find_element_by_id('green_btn').click()
        self.driver.find_element_by_class_name('goto_next').click()
        #3
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #4
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #5
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #6
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #7
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #8
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #9
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #10
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #11
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #12
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #13
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #14
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_xpath("//dd[contains(@class,'select_answer')]/ul/li[2]/input").click()
        self.driver.find_element_by_class_name('goto_next').click()
        #15
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #16
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #17
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #18
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #19
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #20
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #21
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #22
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #23
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #24
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #25
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #26
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #27
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #28
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//dd[contains(@class,'select_answer')]/ul/li/input")))
        element.click()
        self.driver.find_element_by_id("save_paper").click()
























    def lactationweek6to12(self):pass

    def lactationweek12to16(self):pass

    def lactationweek16(self):pass

    def lactationweek20to24(self):pass

    def lactationweek28to32(self):pass

    def habitandcatering(self):pass

    def sportandnutritioninadolescence(self):pass







    def test1_1_renew (self):
        '''renew assessment contains medical.
        '''
        self.driver.find_element_by_xpath("//menu/div/a[1]").click()
        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button").submit()
        self.driver.find_element_by_xpath('//menu/div/a').click()

    def test1_1_renew_medicalFail (self):
        '''renew assessment contains medical.
        '''
        self.driver.find_element_by_xpath("//menu/div/a[1]").click()
        
        self.driver.find_element_by_id('2').click()

        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button[1]").click()
        self.driver.find_element_by_xpath('//menu/div/a').click()

    def first_ (self):
        '''renew assessment contains medical.
        '''
        #self.driver.find_element_by_xpath("//menu/div/a[1]").click()
        
        #self.driver.find_element_by_id('2').click()

        #self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button[1]").click()
        self.driver.find_element_by_xpath('//menu/div/a[2]').click()





    def test1_1_renew_medicalErr (self):
        '''renew assessment contains medical.
        '''
        self.driver.find_element_by_xpath("//menu/div/a[1]").click()
        
        self.driver.find_element_by_id('23').click()

        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button[1]").click()
        self.driver.find_element_by_xpath('//menu/div/a').click()



    def test1_2_new(self):
        '''pass : continue to assess
        '''

        self.driver.find_element_by_xpath("//div/a[2]").click()


    def test1_3_result(self):
        '''nopass:check the last result of the assessment
        '''

        self.driver.find_element_by_xpath("//div/a[3]").click()
         
    def test2(self,height,weight,bodyfat,bodywater,visceralfat,skeletalmuscle,osteocalcin,bmr):
        '''pass
        '''

        self.driver.find_element_by_id('height').send_keys(height)
        self.driver.find_element_by_id('weight').send_keys(weight)
        self.driver.find_element_by_id('body_fat').send_keys(bodyfat)
        self.driver.find_element_by_id('water').send_keys(bodywater)
        self.driver.find_element_by_id('visceral_fat').send_keys(visceralfat)
        self.driver.find_element_by_id('skeletal_muscle').send_keys(skeletalmuscle)
        self.driver.find_element_by_id('osteocalcin').send_keys(osteocalcin)
        self.driver.find_element_by_id('bmr').send_keys(bmr)

        self.driver.find_element_by_class_name('green_btn').click()

    def test3 (self,upperlimb,lower_limb,shoulder1,shoulder2,armgirth,waistline,thighgirth,bustgirth,hipline,calfgirth,frontarm,chest,epigastric,afterarm,back,abdominal,sidewaist,frontthigh,innerthigh,lumbar,afterthigh,outthigh):
        '''no pass 
        '''

        self.driver.find_element_by_id('upper_body_length').send_keys(upperlimb)
        self.driver.find_element_by_id('lower_limb').send_keys (lower_limb)
        self.driver.find_element_by_id('shoulder1').send_keys(shoulder1)
        self.driver.find_element_by_id('shoulder2').send_keys(shoulder2)

        self.driver.find_element_by_id('arm').send_keys(armgirth)
        self.driver.find_element_by_id('waistline').send_keys(waistline)
        self.driver.find_element_by_id('thigh').send_keys(thighgirth)
        self.driver.find_element_by_id('bust').send_keys(bustgirth)
        self.driver.find_element_by_id('hips').send_keys(hipline)
        self.driver.find_element_by_id('calf').send_keys(calfgirth)

        self.driver.find_element_by_id('front_upper_arm').send_keys(frontarm)
        self.driver.find_element_by_id('chest').send_keys(chest)
        self.driver.find_element_by_id('epigastric').send_keys(epigastric)
        self.driver.find_element_by_id('after_the_upper_arm').send_keys(afterarm)
        self.driver.find_element_by_id('rear').send_keys(back)
        self.driver.find_element_by_id('abdominal').send_keys(abdominal)
        self.driver.find_element_by_id('side_of_the_waist').send_keys(sidewaist)
        self.driver.find_element_by_id('front_thigh').send_keys(frontthigh)
        self.driver.find_element_by_id('inner_thigh').send_keys(innerthigh)
        self.driver.find_element_by_id('lumbar').send_keys(lumbar)
        self.driver.find_element_by_id('after_thigh').send_keys(afterthigh)
        self.driver.find_element_by_id('out_thigh').send_keys(outthigh)

        self.driver.find_element_by_xpath("//div[contains(@class,'move_on')]/a[2]").click()






    def log_out_evaluation(self):
        '''no pass: log out the current evaluation user
        '''
        self.driver.find_element_by_xpath("//div[contains(@class,'show_user')]/div[1]/a").click()



    def log_out1(self):
        '''pass ：log out the trainer when no evaluation
        '''
        self.driver.find_element_by_xpath("//div[contains(@class,'show_user')]/a").click()

    def log_out2(self):
        '''no pass ：log out the trainer when evaluate
        '''
        self.driver.find_element_by_xpath("//div[contains(@class,'show_user')]/div[2]/a").click()





url01= 'http://testkpad.hbldiy.com/'
user101 = 'gufei@163.com'


#url01 = 'http://kpadpy.hbl365.com'
#user101 = 'jjiaolian1@sina.com'







class test_bodyshow(object):
    '''
    '''
    def case1(self):
        '''login and log out test
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.log_out1()

    def case2(self):
        '''test data 1
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试101',height='159',weight='44.7',b1='1985',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='159',weight='44.7',bodyfat='13',bodywater='40',visceralfat='1',skeletalmuscle='29.5',osteocalcin='0',bmr='1225') 
        t.test3(upperlimb='80',lower_limb='93',shoulder1='93',shoulder2='50',armgirth='22.578',waistline='58.194',thighgirth='46.905',bustgirth='79.5',hipline='81.885',calfgirth='29.892',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()



    def case3(self):
        '''test1 data 2
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试102',height='159',weight='34',b1='1985',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='159',weight='34',bodyfat='14',bodywater='45',visceralfat='2',skeletalmuscle='29.6',osteocalcin='1',bmr='1131.3') 
        t.test3(upperlimb='80',lower_limb='92',shoulder1='42.5',shoulder2='50',armgirth='22.628',waistline='58.294',thighgirth='47.005',bustgirth='81.5',hipline='81.975',calfgirth='29.992',frontarm='1',chest='1',epigastric='2',afterarm='1',back='1',abdominal='3',sidewaist='2',frontthigh='2',innerthigh='1',lumbar='1',afterthigh='0',outthigh='-2') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case3_renew_fail(self):
        '''test1 data 2
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试102',height='159',weight='34',b1='1985',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_1_renew_medicalFail()
        #t.test2(height='159',weight='34',bodyfat='14',bodywater='45',visceralfat='2',skeletalmuscle='29.6',osteocalcin='1',bmr='1131.3') 
        #t.test3(upperlimb='80',lower_limb='92',shoulder1='42.5',shoulder2='50',armgirth='22.628',waistline='58.294',thighgirth='47.005',bustgirth='81.5',hipline='81.975',calfgirth='29.992',frontarm='1',chest='1',epigastric='2',afterarm='1',back='1',abdominal='3',sidewaist='2',frontthigh='2',innerthigh='1',lumbar='1',afterthigh='0',outthigh='-2') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case3_renew_err(self):
        '''test1 data 2
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试102',height='159',weight='34',b1='1985',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_1_renew_medicalErr()
        t.test2(height='159',weight='34',bodyfat='14',bodywater='45',visceralfat='2',skeletalmuscle='29.6',osteocalcin='1',bmr='1131.3') 
        t.test3(upperlimb='80',lower_limb='92',shoulder1='42.5',shoulder2='50',armgirth='22.628',waistline='58.294',thighgirth='47.005',bustgirth='81.5',hipline='81.975',calfgirth='29.992',frontarm='1',chest='1',epigastric='2',afterarm='1',back='1',abdominal='3',sidewaist='2',frontthigh='2',innerthigh='1',lumbar='1',afterthigh='0',outthigh='-2') 
        #t.log_out_evaluation()
        #t.log_out1()





    def case4(self):
        '''test data 3
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试103',height='159',weight='44',b1='1986',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='159',weight='44',bodyfat='15',bodywater='46',visceralfat='3',skeletalmuscle='34.2',osteocalcin='1.52',bmr='1233') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='22.678',waistline='58.694',thighgirth='47.505',bustgirth='83.5',hipline='81.985',calfgirth='30.492',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='2',sidewaist='3',frontthigh='1',innerthigh='3',lumbar='1',afterthigh='0',outthigh='-1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case5(self):
        '''test data 4
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试104',height='159',weight='44',b1='1987',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='159',weight='44',bodyfat='17.9',bodywater='59',visceralfat='4',skeletalmuscle='34.3',osteocalcin='1.53',bmr='1236.7') 
        t.test3(upperlimb='80',lower_limb='90',shoulder1='40.5',shoulder2='50',armgirth='23.168',waistline='58.794',thighgirth='47.905',bustgirth='85.5',hipline='82.485',calfgirth='30.892',frontarm='1',chest='1',epigastric='2',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='2',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case6(self):
        '''test data 5
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试105',height='160',weight='44',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='160',weight='44',bodyfat='18',bodywater='59.9',visceralfat='5',skeletalmuscle='32.3',osteocalcin='1.79',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='89',shoulder1='40',shoulder2='50',armgirth='23.32',waistline='59.46',thighgirth='48.8',bustgirth='88',hipline='83.4',calfgirth='31.68',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='0',outthigh='2') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case7(self):
        '''test data 6
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试106',height='160',weight='44',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='160',weight='44',bodyfat='24',bodywater='60',visceralfat='6',skeletalmuscle='32.4',osteocalcin='1.8',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='87',shoulder1='39.5',shoulder2='50',armgirth='23.71',waistline='59.56',thighgirth='49.2',bustgirth='90',hipline='84',calfgirth='32.08',frontarm='1',chest='1',epigastric='1',afterarm='-1',back='1',abdominal='-1',sidewaist='-1',frontthigh='-1',innerthigh='-1',lumbar='-1',afterthigh='-1',outthigh='-1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case8(self):
        '''test data 7
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试107',height='160',weight='44',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='160',weight='44',bodyfat='24.1',bodywater='60.12',visceralfat='7',skeletalmuscle='38',osteocalcin='2.0',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='86',shoulder1='50',shoulder2='50',armgirth='23.72',waistline='60.06',thighgirth='49.8',bustgirth='92',hipline='84.4',calfgirth='32.68',frontarm='1',chest='1',epigastric='-1',afterarm='1',back='1',abdominal='0',sidewaist='-1',frontthigh='0',innerthigh='0',lumbar='1',afterthigh='0',outthigh='0') 
        #t.log_out_evaluation()
        #t.log_out1()














    def case14(self):
        '''test data 13
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试113',height='100',weight='50',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_1_renew()
        t.test2(height='171',weight='45',bodyfat='35',bodywater='60',visceralfat='13',skeletalmuscle='36.2',osteocalcin='2.2',bmr='1272.6') 
        t.test3(upperlimb='80',lower_limb='80',shoulder1='50',shoulder2='50',armgirth='27.282',waistline='65.486',thighgirth='45.445',bustgirth='80.9',hipline='92.865',calfgirth='31.148',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='-2',innerthigh='0',lumbar='1',afterthigh='0',outthigh='0') 
        #t.log_out_evaluation()
        #t.log_out1()
    
    def case14_renew(self):
        '''test data 13
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试113',height='100',weight='50',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='171',weight='45',bodyfat='35',bodywater='60',visceralfat='13',skeletalmuscle='36.2',osteocalcin='2.2',bmr='1272.6') 
        t.test3(upperlimb='80',lower_limb='80',shoulder1='50',shoulder2='50',armgirth='27.282',waistline='65.486',thighgirth='45.445',bustgirth='80.9',hipline='92.865',calfgirth='31.148',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='-2',innerthigh='0',lumbar='1',afterthigh='0',outthigh='0') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case40(self):
        '''test data 39 RENEW
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试139',height='100',weight='50',b1='1968',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_1_renew()
        t.test2(height='160',weight='70',bodyfat='39',bodywater='60.12',visceralfat='5',skeletalmuscle='50',osteocalcin='2.5',bmr='1399') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='20.82',waistline='62.56',thighgirth='44.7',bustgirth='76.9',hipline='78.5',calfgirth='26.18',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case41(self):
        '''test data 40 RENEW
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'李测试139',height='100',weight='50',b1='1968',b2='2',b3='3',phone='13717607755',email='lijunfeng1@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.test1_2_new()
        t.test2(height='160',weight='70',bodyfat='39',bodywater='60.12',visceralfat='5',skeletalmuscle='50',osteocalcin='2.5',bmr='1399') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='20.82',waistline='62.56',thighgirth='44.7',bustgirth='76.9',hipline='78.5',calfgirth='26.18',frontarm='1',chest='-1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='-1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case42_(self):
        '''test data 40 RENEW
        '''
        t = main_test()
        t.log_in(url01,user101,'123456')
        t.test1(name=u'lifeng',height='100',weight='50',b1='1968',b2='2',b3='3',phone='13717607755',email='lifeng@sina.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.first_()
        t.test2(height='160',weight='70',bodyfat='39',bodywater='60.12',visceralfat='5',skeletalmuscle='50',osteocalcin='2.5',bmr='1399') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='20.82',waistline='62.56',thighgirth='44.7',bustgirth='76.9',hipline='78.5',calfgirth='26.18',frontarm='1',chest='-1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='-1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()



class test_lactationweek1to6(object):
    '''
    '''
    def case1(self):
        t = main_test()
        t.log_in(url01,user101,'hbl123')
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='1985',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.lactationweek1to6(height='168',pre_pregnancy_weight='40',cur_weight='45',his_pressure1='120',his_pressure2='80',cur_pressure1='140',cur_pressure2='90')





t = test_lactationweek1to6()
t.case1()
