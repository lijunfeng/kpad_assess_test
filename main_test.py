#!/usr/bin/env python
#-*- coding=UTF-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from time import sleep
# select b.* from member a inner join respondent b on a.id =b.member_id where a.email ='lijunfeng@hbldiy.com' order by create_time; 

class main_test(object):
    '''
    test
    '''

    def log_in(self,url,user,password):
        '''
        pass and finish.
        '''

        self.driver = webdriver.Chrome()
        self.driver.get(url)
        self.driver.find_element_by_name('email').send_keys(user)
        self.driver.find_element_by_name('passwd').send_keys(password)
        self.driver.find_element_by_class_name('login_btn').submit()
        WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.NAME,"un")))

    def curday(self):
        from time import localtime
        c =localtime()
        curentday = '-'.join([str(c[0]),str(c[1]),str(c[2])])
        return curentday





    def dd(self,date2):
        from time import mktime,strptime
        t = mktime(strptime(self.curday(),'%Y-%m-%d'))-mktime(strptime(date2,'%Y-%m-%d'))
        d = 24*60*60

        return int(t/d)



    def login_client (self,name,sex,height,weight,b1,b2,b3,phone,email,diastolic,systolic,heart,gestation='0',lactation='0'):
        '''
        test1
        no pass and finish
        '''
        if sex =='man':
            self.driver.find_element_by_name('un').send_keys(name)
            self.driver.find_element_by_xpath("//dd/input[1]").click()
            self.driver.find_element_by_name('he').send_keys(height)
            self.driver.find_element_by_name('we').send_keys(weight)
            self.driver.find_element_by_name('ye').send_keys(b1)
            self.driver.find_element_by_name('mo').send_keys(b2)
            self.driver.find_element_by_name('da').send_keys(b3)
            self.driver.find_element_by_name('te').send_keys(phone)
            self.driver.find_element_by_name('em').send_keys(email)
            self.driver.find_element_by_name('sz').send_keys(diastolic)
            self.driver.find_element_by_name('ss').send_keys(systolic)
            self.driver.find_element_by_name('xl').send_keys(heart)
        elif sex =='woman':
            self.driver.find_element_by_name('un').send_keys(name)
            self.driver.find_element_by_xpath("//dd/input[2]").click()
            self.driver.find_element_by_name('he').send_keys(height)
            self.driver.find_element_by_name('we').send_keys(weight)
            self.driver.find_element_by_name('ye').send_keys(b1)
            self.driver.find_element_by_name('mo').send_keys(b2)
            self.driver.find_element_by_name('da').send_keys(b3)
            self.driver.find_element_by_name('te').send_keys(phone)
            self.driver.find_element_by_name('em').send_keys(email)
            self.driver.find_element_by_name('sz').send_keys(diastolic)
            self.driver.find_element_by_name('ss').send_keys(systolic)
            self.driver.find_element_by_name('xl').send_keys(heart)

            g = self.driver.find_element_by_name('yq')
            Select(g).select_by_value(gestation)

            l = self.driver.find_element_by_name('br')
            Select(l).select_by_value(lactation)
        else:
            print 'Erro please select man or woman'

        self.standardW=float((int(height)-100)*0.8-2.5)

        
        days = self.dd('2013-9-2')
        if 2014-int(b1)<45:
            if 0<=days<=90:print 'the number of trainer is 1'
            elif 91<=days<=270:print 'the number of trainer is 2'
            elif days>=271:print 'the number of trainer is 3'
        elif 2014-int(bi)>=45:
            if 0<=days<=180:print 'the number of trainer is 1'
            elif 181<=days<=360:print 'the number of trainer is 2'
            elif day>360:print 'the number of trainer is 3'



        self.driver.find_element_by_class_name('start_test').submit()
        ele = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'l_foot')]/a")))        
        ele.click()
        WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[2]/div/a")))
        WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[3]/div/a")))

    def lactationweek1to6(self,height,pre_pregnancy_weight,cur_weight,his_pressure1,his_pressure2,cur_pressure1,cur_pressure2):
        

        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[2]/div/a")))
        element.click()
        #1
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.ID,'body_height')))
        element.send_keys(height)
        self.driver.find_element_by_id('prepregnancy_weight').send_keys(pre_pregnancy_weight)
        self.driver.find_element_by_id('current_weight').send_keys(cur_weight)
        #self.driver.find_element_by_id('green_btn').click()
        self.driver.find_element_by_class_name('goto_next').click()
        #2
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.ID,'history_pressure1')))
        element.send_keys(his_pressure1)
        self.driver.find_element_by_id('history_pressure2').send_keys(his_pressure2)
        self.driver.find_element_by_id('current_pressure1').send_keys(cur_pressure1)
        self.driver.find_element_by_id('current_pressure2').send_keys(cur_pressure2)
        #self.driver.find_element_by_id('green_btn').click()
        self.driver.find_element_by_class_name('goto_next').click()
        #3
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[3]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #4
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[4]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #5
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[5]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #6
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[6]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #7
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[7]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #8
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[8]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #9
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[9]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #10
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[10]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #11
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[11]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #12
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[12]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #13
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[13]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #14
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[14]//dd/ul/li[1]/input")))
        element.click()
        self.driver.find_element_by_xpath("//div[contains(@class,'paper_list')]/div[14]//dd/ul/li[2]/input").click()
        self.driver.find_element_by_class_name('goto_next').click()
        #15
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[15]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #16
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[16]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #17
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[17]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #18
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[18]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #19
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[19]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #20
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[20]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #21
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[21]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #22
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[22]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #23
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[23]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #24
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[24]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #25
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[25]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #26
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[26]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #27
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[27]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_class_name('goto_next').click()
        #28
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[28]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_id("save_paper").click()



    def lactationweek6to12(self):
        '''
        '''
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[3]/div/a")))
        element.click()
        #sleep(5)
        #self.driver.find_element_by_xpath("//div[contains(@id,'main')]/div[3]/div/a").click()
        #1~32
        for i in range(1,33,1):
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()

        #33
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[33]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_id("save_paper").click()


    def lactationweek12to16(self):
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[4]/div/a")))
        element.click()
        m =[6,9]
        #1 to25
        for i in range(1,26,1):
            if i in m:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li[1]/input" %i)))
                element.click()
                self.driver.find_element_by_xpath("//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li[2]/input" %i).click()
                self.driver.find_element_by_class_name('goto_next').click()
            else:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()

        #26
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[26]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_id("save_paper").click()


    def lactationweek16(self):
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[5]/div/a")))
        element.click()
        
        #1 to21
        for i in range(1,22,1):
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()

        #22
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[22]//dd/ul/li/input")))
        element.click()
        self.driver.find_element_by_id("save_paper").click()




    def lactationweek20to24(self,thirteen=True,thirtytwo=True):
        print 'thirteen=',thirteen,'\n','thirtytwo=',thirtytwo
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[6]/div/a")))
        element.click()
        #1 to12
        for i in range(1,13,1):
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            print i
         
        if thirteen: #13
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[13]//dd/ul/li[1]/input")))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            for i in range(14,25,1):#14~24
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i
        else: #13
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[13]//dd/ul/li[2]/input")))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            for i in range(25,32,1):#25~31
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i

        if thirtytwo:#32
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[32]//dd/ul/li[1]/input")))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            for i in range(33,37,1):#33 36
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i
            #37
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[37]//dd/ul/li/input")))
            element.click()
            self.driver.find_element_by_id("save_paper").click()

        else:#32
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[32]//dd/ul/li[2]/input")))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            for i in range(38,41,1): #38 40
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i
            #41
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[41]//dd/ul/li/input")))
            element.click()
            self.driver.find_element_by_id("save_paper").click()




    def lactationweek28to32(self,one,twelve):
        print 'one =',one,'\n','twelve =',twelve
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[7]/div/a")))
        element.click()
        #1
        if one:#1,2~6
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[1]//dd/ul/li[1]/input")))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            for i in range(2,7,1):
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i

        else:#1,7~11
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[1]//dd/ul/li[2]/input")))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            for i in range(7,12,1):
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i

        if twelve: #12,13~21,29~49?
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[12]//dd/ul/li[1]/input")))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            for i in range(13,22,1):
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i
            for i in range(29,49,1):
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[49]//dd/ul/li/input")))
            element.click()
            self.driver.find_element_by_id("save_paper").click()

        else: #12 22~49
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[12]//dd/ul/li[2]/input")))
            element.click()
            self.driver.find_element_by_class_name('goto_next').click()
            for i in range(22,49,1):
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                print i
            element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[49]//dd/ul/li/input")))
            element.click()
            self.driver.find_element_by_id("save_paper").click()

    def habitandcatering(self,weight):
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[8]/div/a")))
        ActionChains(self.driver).click(element).perform()


        #self.driver.get('http://testkpad.hbldiy.com/apps/paper-start/11/')
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'login_in')]/button")))
        element.click()
        
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.ID,"reduction_weight")))
        element.send_keys(weight)
        self.driver.find_element_by_class_name('goto_next').click()

        if weight > (self.standardW*1.1):
            print 'weight more than the range of standard'
        elif weight < (self.standardW*0.9):
            print 'weight less than the range of standard'
        elif (self.standardW*1.1) > weight > (self.standardW*0.9):
            print 'weight is in the range of standard'

        s =[]
        for i in range(2,1000,1):
            if i ==2:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[2]//dd/ul/li[%d]/input" %two)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if two ==1:s.append(2)
                elif two ==2:s.append(1)
                elif two ==3:s.append(0)
                print s,i
            elif i ==3:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[4]//dd/ul/li[%d]/input" %four)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if four ==1:s.append(2)
                elif four ==2:s.append(1)
                elif four ==3:s.append(0)   
                print s,i

            elif i ==4:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[6]//dd/ul/li[%d]/input" %six)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if six ==1:s.append(2)
                elif six ==2:s.append(0)
                elif six ==3:s.append(1)
                elif six ==4:s.append(1)
                elif six ==5:s.append(0)
                elif six ==6:s.append(2)
                elif six ==7:s.append(0) 
                elif six ==8:s.append(2)
                print s ,i

            elif i ==5:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[7]//dd/ul/li[%d]/input" %seven)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if seven ==1:s.append(1)
                elif seven ==2:s.append(2)
                elif seven ==3:s.append(0)
                elif seven ==4:s.append(0)
                print s,i 

            elif i ==6:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[8]//dd/ul/li[%d]/input" %eight)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if eight ==1:s.append(2)
                elif eight ==2:s.append(1)
                elif eight ==3:s.append(0)   
                print s,i 

            elif i ==7:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[10]//dd/ul/li[%d]/input" %ten)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if ten ==1:s.append(2)
                elif ten ==2:s.append(1)
                elif ten ==3:s.append(0)   
                print s ,i

            elif i ==8:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[11]//dd/ul/li[%d]/input" %eleven)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if eleven ==1:s.append(0)
                elif eleven ==2:s.append(1)
                elif eleven ==3:s.append(2)   
                print s ,i

            elif i ==9:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[12]//dd/ul/li[%d]/input" %twelve)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if twelve ==1:s.append(2)
                elif twelve ==2:s.append(1)
                elif twelve ==3:s.append(0)   
                print s ,i
            elif i ==10:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[13]//dd/ul/li[%d]/input" %thirteen)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if thirteen ==1:s.append(0)
                elif thirteen ==2:s.append(1)
                elif thirteen ==3:s.append(2)  
                print s ,i

            else:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()

        summary =0
        for j in s:
            summary+=j
        print 'summary=',summary

        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'login_in')]/a")))
        element.click()





    def sportandnutritioninadolescence(self,weight,two,four,six,seven,eight,ten,eleven,twelve,thirteen,fourteen,sixteen,seventeen,eighteen,nineteen,twenty,twentyone,twentyfour,twentyfive):
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[9]/div/a")))
        ActionChains(self.driver).click(element).perform()


        #self.driver.get('http://testkpad.hbldiy.com/apps/paper-start/11/')
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'login_in')]/button")))
        element.click()
        
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.ID,"reduction_weight")))
        element.send_keys(weight)
        self.driver.find_element_by_class_name('goto_next').click()
        


        s =[]
        for i in range(2,26,1):
            if i ==2:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[2]//dd/ul/li[%d]/input" %two)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if two ==1:s.append(2)
                elif two ==2:s.append(1)
                elif two ==3:s.append(0)
                print s,i
            elif i ==4:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[4]//dd/ul/li[%d]/input" %four)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if four ==1:s.append(2)
                elif four ==2:s.append(1)
                elif four ==3:s.append(0)   
                print s,i

            elif i ==6:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[6]//dd/ul/li[%d]/input" %six)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if six ==1:s.append(2)
                elif six ==2:s.append(0)
                elif six ==3:s.append(1)
                elif six ==4:s.append(1)
                elif six ==5:s.append(0)
                elif six ==6:s.append(2)
                elif six ==7:s.append(0) 
                elif six ==8:s.append(2)
                print s ,i

            elif i ==7:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[7]//dd/ul/li[%d]/input" %seven)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if seven ==1:s.append(1)
                elif seven ==2:s.append(2)
                elif seven ==3:s.append(0)
                elif seven ==4:s.append(0)
                print s,i 

            elif i ==8:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[8]//dd/ul/li[%d]/input" %eight)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if eight ==1:s.append(2)
                elif eight ==2:s.append(1)
                elif eight ==3:s.append(0)   
                print s,i 

            elif i ==10:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[10]//dd/ul/li[%d]/input" %ten)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if ten ==1:s.append(2)
                elif ten ==2:s.append(1)
                elif ten ==3:s.append(0)   
                print s ,i

            elif i ==11:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[11]//dd/ul/li[%d]/input" %eleven)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if eleven ==1:s.append(0)
                elif eleven ==2:s.append(1)
                elif eleven ==3:s.append(2)   
                print s ,i

            elif i ==12:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[12]//dd/ul/li[%d]/input" %twelve)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if twelve ==1:s.append(2)
                elif twelve ==2:s.append(1)
                elif twelve ==3:s.append(0)   
                print s ,i
            elif i ==13:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[13]//dd/ul/li[%d]/input" %thirteen)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if thirteen ==1:s.append(0)
                elif thirteen ==2:s.append(1)
                elif thirteen ==3:s.append(2)  
                print s ,i
            elif i ==14:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[14]//dd/ul/li[%d]/input" %fourteen)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if fourteen ==1:s.append(2)
                elif fourteen ==2:s.append(1)
                elif fourteen ==3:s.append(0)   
                print s ,i
            elif i ==16:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[16]//dd/ul/li[%d]/input" %sixteen)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if sixteen ==1:s.append(0)
                elif sixteen ==2:s.append(1)
                elif sixteen ==3:s.append(2)  
                print s ,i
            elif i ==17:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[17]//dd/ul/li[%d]/input" %seventeen)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if seventeen ==1:s.append(2)
                elif seventeen ==2:s.append(1)
                elif seventeen ==3:s.append(0)   
                print s ,i
            elif i ==18:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[18]//dd/ul/li[%d]/input" %eighteen)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if eighteen ==1:s.append(0)
                elif eighteen ==2:s.append(1)
                elif eighteen ==3:s.append(2) 
                print s ,i
            elif i ==19:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[19]//dd/ul/li[%d]/input" %nineteen)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if nineteen ==1:s.append(0)
                elif nineteen ==2:s.append(1)
                elif nineteen ==3:s.append(2) 
                print s ,i
            elif i ==20:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[20]//dd/ul/li[%d]/input" %twenty)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if twenty ==1:s.append(0)
                elif twenty ==2:s.append(1)
                elif twenty ==3:s.append(2) 
                print s ,i
            elif i ==21:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[21]//dd/ul/li[%d]/input" %twentyone)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if twentyone ==1:s.append(2)
                elif twentyone ==2:s.append(0)
                elif twentyone ==3:s.append(0) 
                print s ,i
            elif i ==24:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[24]//dd/ul/li[%d]/input" %twentyfour)))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()
                if twentyfour ==1:s.append(2)
                elif twentyfour ==2:s.append(1)
                elif twentyfour ==3:s.append(0) 
                print s ,i
            elif i ==25:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[25]//dd/ul/li[%d]/input" %twentyfive)))
                element.click()
                self.driver.find_element_by_id("save_paper").click()
                if twentyfive ==1:s.append(0)
                elif twentyfive ==2:s.append(2)
                elif twentyfive ==3:s.append(1)  
                print s ,i

            else:
                element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'paper_list')]/div[%d]//dd/ul/li/input" %i )))
                element.click()
                self.driver.find_element_by_class_name('goto_next').click()

        summary =0
        for j in s:
            summary+=j
        print 'summary=',summary

        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@class,'login_in')]/a")))
        element.click()






































#---------BodyShow------------------------
    def bodyshow_new_medical5(self):
        '''the first assessiing ande the part of medical is 5 
        '''
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        element.click()

        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button").submit()
        
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//menu/div/a")))
        element.click()       



        #self.driver.find_element_by_xpath('//menu/div/a').click()

    def bodyshow_new_medical3(self):
        '''the first assessiing ande the part of medical is 3
        '''
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        element.click()
        self.driver.find_element_by_id('23').click()
        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button[1]").click()
        
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//menu/div/a")))
        element.click()  



        #self.driver.find_element_by_xpath('//menu/div/a').click()

    def bodyshow_new_medicalFai(self):
        '''the first assessiing ande the part of medical is fail
        '''
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        element.click()
        self.driver.find_element_by_id('2').click()
        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button[1]").click()
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//menu/div/a")))
        element.click()  



        #self.driver.find_element_by_xpath('//menu/div/a').click()


    def bodyshow_renew_medical5(self):
        '''
        '''
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        element.click()

        self.driver.find_element_by_xpath("//menu/div/a[1]").click()
        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button").submit()
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//menu/div/a")))
        element.click()  



        #self.driver.find_element_by_xpath('//menu/div/a').click()

    def bodyshow_renew_medica3(self):
        '''
        '''
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        element.click()

        self.driver.find_element_by_xpath("//menu/div/a[1]").click()
        self.driver.find_element_by_id('23').click()
        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button[1]").click()
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//menu/div/a")))
        element.click()  

        #self.driver.find_element_by_xpath('//menu/div/a').click()

    def bodyshow_renew_medicafail(self):
        '''?????????????????????
        '''
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        element.click()

        self.driver.find_element_by_xpath("//menu/div/a[1]").click()
        self.driver.find_element_by_id('2').click()
        self.driver.find_element_by_xpath("//div[contains(@class,'login_in')]/button[1]").click()
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//menu/div/a")))
        element.click()  

        #self.driver.find_element_by_xpath('//menu/div/a').click()



    def bodyshow_repetion(self):
        '''
        '''
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        element.click()
        element = WebDriverWait(self.driver,10).until(EC.element_to_be_clickable((By.XPATH,"//menu/div/a[2]")))
        element.click()

    def bodyshow_checklast(self):
        '''
        '''
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//div[contains(@id,'main')]/div[1]/div/a")))
        element.click()
        element = WebDriverWait(self.driver,5).until(EC.element_to_be_clickable((By.XPATH,"//menu/div/a[3]")))
        element.click()

#______next part assessing______________________
         
    def test2(self,height,weight,bodyfat,bodywater,visceralfat,skeletalmuscle,osteocalcin,bmr):
        '''pass
        '''

        self.driver.find_element_by_id('height').send_keys(height)
        self.driver.find_element_by_id('weight').send_keys(weight)
        self.driver.find_element_by_id('body_fat').send_keys(bodyfat)
        self.driver.find_element_by_id('water').send_keys(bodywater)
        self.driver.find_element_by_id('visceral_fat').send_keys(visceralfat)
        self.driver.find_element_by_id('skeletal_muscle').send_keys(skeletalmuscle)
        self.driver.find_element_by_id('osteocalcin').send_keys(osteocalcin)
        self.driver.find_element_by_id('bmr').send_keys(bmr)
            
        self.driver.find_element_by_class_name('green_btn').click()

    def test2_check(self,height,weight,bodyfat,bodywater,visceralfat,skeletalmuscle,osteocalcin,bmr):
        '''pass
        '''

        self.driver.find_element_by_id('height').send_keys(height)
        self.driver.find_element_by_id('weight').send_keys(weight)
        self.driver.find_element_by_id('body_fat').send_keys(bodyfat)
        self.driver.find_element_by_id('water').send_keys(bodywater)
        self.driver.find_element_by_id('visceral_fat').send_keys(visceralfat)
        self.driver.find_element_by_id('skeletal_muscle').send_keys(skeletalmuscle)
        self.driver.find_element_by_id('osteocalcin').send_keys(osteocalcin)
        self.driver.find_element_by_id('bmr').send_keys(bmr)

        self.driver.find_element_by_class_name('yellow_btn').click()
        sleep(10)
        self.driver.find_element_by_path("//div[contains(@class,'colsed_btn')]/a").click()

#-------------the third paty assessing --------

    def test3 (self,upperlimb,lower_limb,shoulder1,shoulder2,armgirth,waistline,thighgirth,bustgirth,hipline,calfgirth,frontarm,chest,epigastric,afterarm,back,abdominal,sidewaist,frontthigh,innerthigh,lumbar,afterthigh,outthigh):
        '''no pass 
        '''

        self.driver.find_element_by_id('upper_body_length').send_keys(upperlimb)
        self.driver.find_element_by_id('lower_limb').send_keys (lower_limb)
        self.driver.find_element_by_id('shoulder1').send_keys(shoulder1)
        self.driver.find_element_by_id('shoulder2').send_keys(shoulder2)

        self.driver.find_element_by_id('arm').send_keys(armgirth)
        self.driver.find_element_by_id('waistline').send_keys(waistline)
        self.driver.find_element_by_id('thigh').send_keys(thighgirth)
        self.driver.find_element_by_id('bust').send_keys(bustgirth)
        self.driver.find_element_by_id('hips').send_keys(hipline)
        self.driver.find_element_by_id('calf').send_keys(calfgirth)

        self.driver.find_element_by_id('front_upper_arm').send_keys(frontarm)
        self.driver.find_element_by_id('chest').send_keys(chest)
        self.driver.find_element_by_id('epigastric').send_keys(epigastric)
        self.driver.find_element_by_id('after_the_upper_arm').send_keys(afterarm)
        self.driver.find_element_by_id('rear').send_keys(back)
        self.driver.find_element_by_id('abdominal').send_keys(abdominal)
        self.driver.find_element_by_id('side_of_the_waist').send_keys(sidewaist)
        self.driver.find_element_by_id('front_thigh').send_keys(frontthigh)
        self.driver.find_element_by_id('inner_thigh').send_keys(innerthigh)
        self.driver.find_element_by_id('lumbar').send_keys(lumbar)
        self.driver.find_element_by_id('after_thigh').send_keys(afterthigh)
        self.driver.find_element_by_id('out_thigh').send_keys(outthigh)

        self.driver.find_element_by_xpath("//div[contains(@class,'move_on')]/a[2]").click()

    def test3_check (self,upperlimb,lower_limb,shoulder1,shoulder2,armgirth,waistline,thighgirth,bustgirth,hipline,calfgirth,frontarm,chest,epigastric,afterarm,back,abdominal,sidewaist,frontthigh,innerthigh,lumbar,afterthigh,outthigh):
        '''no pass 
        '''

        self.driver.find_element_by_id('upper_body_length').send_keys(upperlimb)
        self.driver.find_element_by_id('lower_limb').send_keys (lower_limb)
        self.driver.find_element_by_id('shoulder1').send_keys(shoulder1)
        self.driver.find_element_by_id('shoulder2').send_keys(shoulder2)

        self.driver.find_element_by_id('arm').send_keys(armgirth)
        self.driver.find_element_by_id('waistline').send_keys(waistline)
        self.driver.find_element_by_id('thigh').send_keys(thighgirth)
        self.driver.find_element_by_id('bust').send_keys(bustgirth)
        self.driver.find_element_by_id('hips').send_keys(hipline)
        self.driver.find_element_by_id('calf').send_keys(calfgirth)

        self.driver.find_element_by_id('front_upper_arm').send_keys(frontarm)
        self.driver.find_element_by_id('chest').send_keys(chest)
        self.driver.find_element_by_id('epigastric').send_keys(epigastric)
        self.driver.find_element_by_id('after_the_upper_arm').send_keys(afterarm)
        self.driver.find_element_by_id('rear').send_keys(back)
        self.driver.find_element_by_id('abdominal').send_keys(abdominal)
        self.driver.find_element_by_id('side_of_the_waist').send_keys(sidewaist)
        self.driver.find_element_by_id('front_thigh').send_keys(frontthigh)
        self.driver.find_element_by_id('inner_thigh').send_keys(innerthigh)
        self.driver.find_element_by_id('lumbar').send_keys(lumbar)
        self.driver.find_element_by_id('after_thigh').send_keys(afterthigh)
        self.driver.find_element_by_id('out_thigh').send_keys(outthigh)

        self.driver.find_element_by_class_name('yellow_btn').click()
        sleep(10)
        self.driver.find_element_by_xpath("//div[contains(@class,'colsed_btn')]/a").click




    def log_out_evaluation(self):
        '''no pass: log out the current evaluation user
        '''
        self.driver.find_element_by_xpath("//div[contains(@class,'show_user')]/div[1]/a").click()



    def log_out1(self):
        '''pass ：log out the trainer when no evaluation
        '''
        self.driver.find_element_by_xpath("//div[contains(@class,'show_user')]/a").click()

    def log_out2(self):
        '''no pass ：log out the trainer when evaluate
        '''
        self.driver.find_element_by_xpath("//div[contains(@class,'show_user')]/div[2]/a").click()







