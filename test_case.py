#!/usr/bin/env python
#-*- coding=UTF-8 -*-
from main_test import main_test



url_1= u'http://ihealth.kangkanggou.com/'
user_1 = u'gufei@163.com'
password = u'hbl123'

#url_1 = 'http://kpadpy.hbl365.com'
#user_1 = 'jjiaolian1@sina.com'
#password = password

class test_case_bodyshow(object):
    '''
    '''
    def case1(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试101',sex=u'woman',height='159',weight='44.7',b1='1985',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='159',weight='44.7',bodyfat='13',bodywater='40',visceralfat='1',skeletalmuscle='29.5',osteocalcin='0',bmr='1225') 
        t.test3(upperlimb='80',lower_limb='93',shoulder1='93',shoulder2='50',armgirth='22.578',waistline='58.194',thighgirth='46.905',bustgirth='79.5',hipline='81.885',calfgirth='29.892',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case1_(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试101',sex=u'woman',height='159',weight='44.7',b1='1985',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_new_medical3()


        t.test2(height='159',weight='44.7',bodyfat='13',bodywater='40',visceralfat='1',skeletalmuscle='29.5',osteocalcin='0',bmr='1225') 
        t.test3(upperlimb='80',lower_limb='93',shoulder1='93',shoulder2='50',armgirth='22.578',waistline='58.194',thighgirth='46.905',bustgirth='79.5',hipline='81.885',calfgirth='29.892',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case2(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试102',sex=u'woman',height='159',weight='34',b1='1985',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='159',weight='34',bodyfat='14',bodywater='45',visceralfat='2',skeletalmuscle='29.6',osteocalcin='1',bmr='1131.3') 
        t.test3(upperlimb='80',lower_limb='92',shoulder1='42.5',shoulder2='50',armgirth='22.628',waistline='58.294',thighgirth='47.005',bustgirth='81.5',hipline='81.975',calfgirth='29.992',frontarm='1',chest='1',epigastric='2',afterarm='1',back='1',abdominal='3',sidewaist='2',frontthigh='2',innerthigh='1',lumbar='1',afterthigh='0',outthigh='-2') 
        #t.log_out_evaluation()
        #t.log_out1()



    def case3(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试103',sex=u'woman',height='159',weight='44',b1='1986',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='159',weight='44',bodyfat='15',bodywater='46',visceralfat='3',skeletalmuscle='34.2',osteocalcin='1.52',bmr='1233') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='22.678',waistline='58.694',thighgirth='47.505',bustgirth='83.5',hipline='81.985',calfgirth='30.492',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='2',sidewaist='3',frontthigh='1',innerthigh='3',lumbar='1',afterthigh='0',outthigh='-1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case4(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试104',sex=u'woman',height='159',weight='44',b1='1987',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='159',weight='44',bodyfat='17.9',bodywater='59',visceralfat='4',skeletalmuscle='34.3',osteocalcin='1.53',bmr='1236.7') 
        t.test3(upperlimb='80',lower_limb='90',shoulder1='40.5',shoulder2='50',armgirth='23.168',waistline='58.794',thighgirth='47.905',bustgirth='85.5',hipline='82.485',calfgirth='30.892',frontarm='1',chest='1',epigastric='2',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='2',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case5(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试105',sex=u'woman',height='160',weight='44',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='44',bodyfat='18',bodywater='59.9',visceralfat='5',skeletalmuscle='32.3',osteocalcin='1.79',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='89',shoulder1='40',shoulder2='50',armgirth='23.32',waistline='59.46',thighgirth='48.8',bustgirth='88',hipline='83.4',calfgirth='31.68',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='0',outthigh='2') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case6(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试106',sex=u'woman',height='160',weight='44',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='44',bodyfat='24',bodywater='60',visceralfat='6',skeletalmuscle='32.4',osteocalcin='1.8',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='87',shoulder1='39.5',shoulder2='50',armgirth='23.71',waistline='59.56',thighgirth='49.2',bustgirth='90',hipline='84',calfgirth='32.08',frontarm='1',chest='1',epigastric='-1',afterarm='-1',back='1',abdominal='-1',sidewaist='-1',frontthigh='-1',innerthigh='-1',lumbar='-1',afterthigh='-1',outthigh='-1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case7(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试107',sex=u'woman',height='160',weight='44',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='44',bodyfat='24.1',bodywater='60.12',visceralfat='7',skeletalmuscle='38',osteocalcin='2.0',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='86',shoulder1='50',shoulder2='50',armgirth='23.72',waistline='60.06',thighgirth='49.8',bustgirth='92',hipline='84.4',calfgirth='32.68',frontarm='1',chest='1',epigastric='-1',afterarm='1',back='1',abdominal='0',sidewaist='-1',frontthigh='0',innerthigh='0',lumbar='1',afterthigh='0',outthigh='0') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case8(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试108',sex=u'woman',height='160',weight='45',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='45',bodyfat='27',bodywater='40',visceralfat='8',skeletalmuscle='38.1',osteocalcin='0',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='85',shoulder1='42.5',shoulder2='50',armgirth='24.31',waistline='60.2',thighgirth='50.2',bustgirth='94',hipline='85',calfgirth='33.08',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case9(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试109',sex=u'woman',height='170',weight='45',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='170',weight='45',bodyfat='28',bodywater='45',visceralfat='9',skeletalmuscle='32.3',osteocalcin='1.86',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='84',shoulder1='42',shoulder2='50',armgirth='25.74',waistline='64.1',thighgirth='53.75',bustgirth='101',hipline='90.6',calfgirth='35.56',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='0',lumbar='1',afterthigh='1',outthigh='1' ) 
        #t.log_out_evaluation()
        #t.log_out1()


    def case10(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试110',sex=u'woman',height='170',weight='45',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='170',weight='45',bodyfat='28.1',bodywater='46',visceralfat='10',skeletalmuscle='32.4',osteocalcin='1.87',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='83',shoulder1='40.5',shoulder2='50',armgirth='26.13',waistline='64.2',thighgirth='54.15',bustgirth='103',hipline='91.2',calfgirth='35.96',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='0',innerthigh='0',lumbar='1',afterthigh='1',outthigh='1'  ) 
        #t.log_out_evaluation()
        #t.log_out1()


    def case11(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试111',sex=u'woman',height='170',weight='45',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='170',weight='45',bodyfat='34',bodywater='59',visceralfat='11',skeletalmuscle='38',osteocalcin='2',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='82',shoulder1='40',shoulder2='50',armgirth='26.14',waistline='64.7',thighgirth='54.85',bustgirth='106',hipline='91.6',calfgirth='36.66',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1'   ) 
        #t.log_out_evaluation()
        #t.log_out1()


    def case12(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试112',sex=u'woman',height='170',weight='45',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='170',weight='45',bodyfat='34.1',bodywater='59.9',visceralfat='12',skeletalmuscle='38.1',osteocalcin='2.1',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='81',shoulder1='39.5',shoulder2='50',armgirth='26.74',waistline='64.8',thighgirth='54.95',bustgirth='80',hipline='92.3',calfgirth='31.06',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1'   ) 
        #t.log_out_evaluation()
        #t.log_out1()


    def case13(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试113',sex=u'woman',height='171',weight='45',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='171',weight='45',bodyfat='35',bodywater='60',visceralfat='13',skeletalmuscle='36.2',osteocalcin='2.2',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='80',shoulder1='50',shoulder2='50',armgirth='27.282',waistline='65.5',thighgirth='45.445',bustgirth='80.9',hipline='92.9',calfgirth='31.148',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='-2',innerthigh='0',lumbar='1',afterthigh='0',outthigh='0') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case14(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试114',sex=u'woman',height='171',weight='45',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='171',weight='45',bodyfat='13',bodywater='60.12',visceralfat='14',skeletalmuscle='36.3',osteocalcin='2.3',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='92',shoulder1='42.5',shoulder2='50',armgirth='27.882',waistline='65.6',thighgirth='45.845',bustgirth='81.4',hipline='78.1',calfgirth='30.148',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='3',innerthigh='-1',lumbar='1',afterthigh='0',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case15(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试115',sex=u'woman',height='171',weight='50',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='171',weight='50',bodyfat='14',bodywater='40',visceralfat='15',skeletalmuscle='42.7',osteocalcin='0',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='28.282',waistline='66.1',thighgirth='46.445',bustgirth='81.9',hipline='77.1',calfgirth='29.148',frontarm='3',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='0',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case16(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试116',sex=u'woman',height='171',weight='50',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='171',weight='50',bodyfat='17.9',bodywater='45',visceralfat='16',skeletalmuscle='42.8',osteocalcin='1.86',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='90',shoulder1='40.5',shoulder2='50',armgirth='28.982',waistline='66.2',thighgirth='46.845',bustgirth='88.5',hipline='79.1',calfgirth='28.148',frontarm='-2',chest='1',epigastric='1',afterarm='-1',back='1',abdominal='1',sidewaist='1',frontthigh='0',innerthigh='0',lumbar='1',afterthigh='0',outthigh='-1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case17(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试117',sex=u'woman',height='160',weight='55',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='55',bodyfat='18',bodywater='46',visceralfat='17',skeletalmuscle='50',osteocalcin='1.87',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='89',shoulder1='40',shoulder2='50',armgirth='11.72',waistline='62.5',thighgirth='44.2',bustgirth='77.4',hipline='74.4',calfgirth='25.08',frontarm='2',chest='1',epigastric='1',afterarm='2',back='1',abdominal='1',sidewaist='1',frontthigh='-1',innerthigh='-1',lumbar='1',afterthigh='-1',outthigh='-1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case18(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试118',sex=u'woman',height='160',weight='50',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='50',bodyfat='24',bodywater='59',visceralfat='18',skeletalmuscle='50',osteocalcin='2',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='87',shoulder1='39.5',shoulder2='50',armgirth='12.72',waistline='62.6',thighgirth='44.6',bustgirth='78',hipline='75.4',calfgirth='24.08',frontarm='-1',chest='1',epigastric='1',afterarm='-1',back='1',abdominal='1',sidewaist='1',frontthigh='-2',innerthigh='3',lumbar='1',afterthigh='-2',outthigh='-2') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case19(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试119',sex=u'woman',height='160',weight='50',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='50',bodyfat='28',bodywater='59.9',visceralfat='19',skeletalmuscle='50',osteocalcin='2.1',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='86',shoulder1='50',shoulder2='50',armgirth='13.72',waistline='63.2',thighgirth='45.2',bustgirth='78.4',hipline='76.4',calfgirth='23.08',frontarm='-2',chest='1',epigastric='1',afterarm='-1',back='1',abdominal='1',sidewaist='1',frontthigh='0',innerthigh='0',lumbar='1',afterthigh='-1',outthigh='-1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case20(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试120',sex=u'woman',height='160',weight='50',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='50',bodyfat='28.1',bodywater='60',visceralfat='1',skeletalmuscle='50',osteocalcin='2.2',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='85',shoulder1='42.5',shoulder2='50',armgirth='14.72',waistline='63.3',thighgirth='45.6',bustgirth='79',hipline='77.4',calfgirth='22.08',frontarm='2',chest='1',epigastric='1',afterarm='-1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case21(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试121',sex=u'woman',height='160',weight='50',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='50',bodyfat='29',bodywater='60.12',visceralfat='2',skeletalmuscle='50',osteocalcin='2.3',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='84',shoulder1='42',shoulder2='50',armgirth='15.72',waistline='63.4',thighgirth='46.2',bustgirth='79.4',hipline='78.4',calfgirth='21.08',frontarm='3',chest='1',epigastric='1',afterarm='2',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()
    
    def case22(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试122',sex=u'woman',height='160',weight='60',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='60',bodyfat='34',bodywater='40',visceralfat='3',skeletalmuscle='50',osteocalcin='0',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='83',shoulder1='40.5',shoulder2='50',armgirth='16.72',waistline='57.6',thighgirth='46.6',bustgirth='79.9',hipline='79.4',calfgirth='20.08',frontarm='1',chest='1',epigastric='-2',afterarm='1',back='1',abdominal='-2',sidewaist='-2',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case23(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试123',sex=u'woman',height='160',weight='60',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='60',bodyfat='34.1',bodywater='45',visceralfat='4',skeletalmuscle='50',osteocalcin='1.86',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='82',shoulder1='40',shoulder2='50',armgirth='17.72',waistline='58.7',thighgirth='47.1',bustgirth='80',hipline='80.4',calfgirth='30.09',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case24(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试124',sex=u'woman',height='160',weight='60',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='60',bodyfat='38',bodywater='46',visceralfat='5',skeletalmuscle='50',osteocalcin='1.87',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='81',shoulder1='39.5',shoulder2='50',armgirth='18.72',waistline='59.1',thighgirth='47.21',bustgirth='81',hipline='81.4',calfgirth='30.17',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()



    def case25(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试125',sex=u'woman',height='160',weight='60',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='60',bodyfat='38.1',bodywater='59',visceralfat='6',skeletalmuscle='50',osteocalcin='2',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='93',shoulder1='50',shoulder2='50',armgirth='19.72',waistline='59.2',thighgirth='47.7',bustgirth='83',hipline='82.5',calfgirth='30.28',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case26(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试126',sex=u'woman',height='160',weight='60',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='60',bodyfat='39',bodywater='59.9',visceralfat='7',skeletalmuscle='50',osteocalcin='2.1',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='92',shoulder1='42.5',shoulder2='50',armgirth='20.72',waistline='59.5',thighgirth='48.1',bustgirth='85',hipline='82.6',calfgirth='30.98',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case27(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试127',sex=u'woman',height='160',weight='60',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='60',bodyfat='13',bodywater='60',visceralfat='8',skeletalmuscle='50',osteocalcin='2.2',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='21.72',waistline='59.6',thighgirth='48.7',bustgirth='87',hipline='82.9',calfgirth='31.18',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()


    def case28(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试128',sex=u'woman',height='160',weight='60',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='60',bodyfat='14',bodywater='60.12',visceralfat='9',skeletalmuscle='50',osteocalcin='2.3',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='90',shoulder1='40.5',shoulder2='50',armgirth='21.82',waistline='60.1',thighgirth='49.1',bustgirth='89',hipline='83.1',calfgirth='31.98',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case29(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试129',sex=u'woman',height='160',weight='61',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='61',bodyfat='17.9',bodywater='40',visceralfat='10',skeletalmuscle='50',osteocalcin='0',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='89',shoulder1='40',shoulder2='50',armgirth='12.82',waistline='60.2',thighgirth='49.7',bustgirth='91',hipline='83.3',calfgirth='32.18',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()
    
    def case30(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试130',sex=u'woman',height='160',weight='62',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='62',bodyfat='18',bodywater='45',visceralfat='11',skeletalmuscle='50',osteocalcin='2.124',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='87',shoulder1='39.5',shoulder2='50',armgirth='13.82',waistline='60.5',thighgirth='50.1',bustgirth='93',hipline='83.5',calfgirth='32.78',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case31(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试131',sex=u'woman',height='160',weight='63',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='63',bodyfat='24',bodywater='46',visceralfat='12',skeletalmuscle='50',osteocalcin='2.125',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='86',shoulder1='50',shoulder2='50',armgirth='14.82',waistline='60.6',thighgirth='50.7',bustgirth='95',hipline='84.3',calfgirth='33.58',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()
    
    def case32(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试132',sex=u'woman',height='160',weight='64',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='64',bodyfat='28',bodywater='59',visceralfat='13',skeletalmuscle='50',osteocalcin='2.4',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='85',shoulder1='42.5',shoulder2='50',armgirth='15.82',waistline='61.1',thighgirth='51.1',bustgirth='97',hipline='84.5',calfgirth='33.98',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case33(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试133',sex=u'woman',height='160',weight='65',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='65',bodyfat='28.1',bodywater='59.9',visceralfat='14',skeletalmuscle='50',osteocalcin='2.5',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='84',shoulder1='42',shoulder2='50',armgirth='16.82',waistline='61.2',thighgirth='51.8',bustgirth='99',hipline='85.3',calfgirth='34.68',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case34(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试134',sex=u'woman',height='160',weight='66',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='66',bodyfat='29',bodywater='60',visceralfat='15',skeletalmuscle='50',osteocalcin='2.6',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='83',shoulder1='40.5',shoulder2='50',armgirth='17.82',waistline='61.5',thighgirth='46.7',bustgirth='79.5',hipline='84.9',calfgirth='20.18',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case35(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试135',sex=u'woman',height='160',weight='67',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='67',bodyfat='34',bodywater='60.12',visceralfat='16',skeletalmuscle='50',osteocalcin='0',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='82',shoulder1='40',shoulder2='50',armgirth='18.82',waistline='61.6',thighgirth='46.3',bustgirth='79.1',hipline='85.1',calfgirth='21.98',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case36(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试136',sex=u'woman',height='160',weight='68',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='68',bodyfat='34.1',bodywater='59',visceralfat='17',skeletalmuscle='50',osteocalcin='2.124',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='81',shoulder1='39.5',shoulder2='50',armgirth='19.82',waistline='62.1',thighgirth='42.7',bustgirth='78.5',hipline='85.9',calfgirth='22.98',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case37(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试137',sex=u'woman',height='160',weight='69',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='69',bodyfat='38',bodywater='59.9',visceralfat='18',skeletalmuscle='50',osteocalcin='2.125',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='93',shoulder1='50',shoulder2='50',armgirth='20.82',waistline='62.2',thighgirth='43.3',bustgirth='78.1',hipline='74.3',calfgirth='24.18',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()
    def case38(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试138',sex=u'woman',height='160',weight='41',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='41',bodyfat='24',bodywater='60',visceralfat='19',skeletalmuscle='29.6',osteocalcin='1.8',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='92',shoulder1='42.5',shoulder2='50',armgirth='22.72',waistline='56.6',thighgirth='47.2',bustgirth='80',hipline='82.4',calfgirth='30.08',frontarm='1',chest='1',epigastric='0',afterarm='1',back='1',abdominal='0',sidewaist='-1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case39(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试139',sex=u'woman',height='160',weight='70',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='70',bodyfat='39',bodywater='60.12',visceralfat='5',skeletalmuscle='50',osteocalcin='2.5',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='20.82',waistline='62.6',thighgirth='44.7',bustgirth='76.9',hipline='78.5',calfgirth='26.18',frontarm='1',chest='1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case40(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试140',sex=u'woman',height='160',weight='70',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')
        t.bodyshow_repetion()
        t.test2(height='160',weight='70',bodyfat='39',bodywater='60.12',visceralfat='5',skeletalmuscle='50',osteocalcin='2.5',bmr='2000') 
        t.test3(upperlimb='80',lower_limb='91',shoulder1='42',shoulder2='50',armgirth='20.82',waistline='62.6',thighgirth='44.7',bustgirth='76.9',hipline='78.5',calfgirth='26.18',frontarm='1',chest='-1',epigastric='1',afterarm='1',back='1',abdominal='1',sidewaist='1',frontthigh='1',innerthigh='1',lumbar='1',afterthigh='-1',outthigh='1') 
        #t.log_out_evaluation()
        #t.log_out1()

    def case41(self):
        '''test data
        '''
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'李测试140',sex=u'woman',height='160',weight='70',b1='1988',b2='2',b3='3',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='65',gestation='0',lactation='0')


#t = test_case_bodyshow()
#t.case18()

#t = test_case_bodyshow()
#t.case20()

#t = test_case_bodyshow()
#t.case38()

#t = test_case_bodyshow()
#t.case22()


t = test_case_bodyshow()
t.case1_()

#----------------------------------------------------------------------------the Other Assessing------------------------------------------------------------------------------------------------------


class test_case_lactationweek1to6(object):
    '''
    '''
    def case1(self):
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='1985',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.lactationweek1to6(height='168',pre_pregnancy_weight='40',cur_weight='45',his_pressure1='120',his_pressure2='80',cur_pressure1='140',cur_pressure2='90')


class test_case_lactationweek6to12(object):
    '''
    '''
    def case1(self):
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='1985',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.lactationweek6to12()

class test_case_lactationweek12to16(object):
    '''
    '''
    def case1(self):
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='1985',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.lactationweek12to16()

class test_case_lactationweek16(object):
    '''
    '''
    def case1(self):
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='1985',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.lactationweek16()

class test_case_lactationweek20to24(object):
    '''
    '''

    def case1(self):
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='1985',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.lactationweek20to24(True,True) #13~24    -   32~37
        t.lactationweek20to24(False,False)#13-   25~31   -32 -  38~41
        t.lactationweek20to24(True,False)
        t.lactationweek20to24(False,True)

class test_case_lactationweek28to32(object):
    '''
    '''
    def case1(self):
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='1985',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.lactationweek28to32(True,True) #1314
        t.lactationweek28to32(False,False)
        t.lactationweek28to32(False,True)
        t.lactationweek28to32(True,False)





class test_case_sportandnutritioninadolescence(object):

    def case1(self): #0
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='2000',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.sportandnutritioninadolescence(weight='45',two=3,four=3,six=2,seven=3,eight=3,ten=3,eleven=1,twelve=3,thirteen=1,fourteen=3,sixteen=1,seventeen=3,eighteen=1,nineteen=1,twenty=1,twentyone=3,twentyfour=3,twentyfive=1)

    def case2(self):#17
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='2000',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.sportandnutritioninadolescence(weight='45',two=2,four=2,six=3,seven=1,eight=2,ten=2,eleven=2,twelve=2,thirteen=2,fourteen=2,sixteen=2,seventeen=2,eighteen=2,nineteen=2,twenty=2,twentyone=1,twentyfour=3,twentyfive=1)

    def case3(self):#18
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='2000',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.sportandnutritioninadolescence(weight='45',two=2,four=2,six=3,seven=1,eight=2,ten=2,eleven=2,twelve=2,thirteen=2,fourteen=2,sixteen=2,seventeen=2,eighteen=2,nineteen=2,twenty=2,twentyone=1,twentyfour=3,twentyfive=3)

    def case4(self):#27
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='2000',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.sportandnutritioninadolescence(weight='45',two=1,four=1,six=1,seven=1,eight=1,ten=1,eleven=3,twelve=1,thirteen=3,fourteen=1,sixteen=3,seventeen=1,eighteen=3,nineteen=2,twenty=1,twentyone=3,twentyfour=3,twentyfive=1)

    def case5(self):#28
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='2000',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.sportandnutritioninadolescence(weight='45',two=1,four=1,six=1,seven=1,eight=1,ten=1,eleven=3,twelve=1,thirteen=3,fourteen=1,sixteen=3,seventeen=1,eighteen=3,nineteen=3,twenty=1,twentyone=3,twentyfour=3,twentyfive=1)

    def case6(self):#36
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='2000',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.sportandnutritioninadolescence(weight='45',two=1,four=1,six=1,seven=1,eight=1,ten=1,eleven=3,twelve=1,thirteen=3,fourteen=1,sixteen=3,seventeen=1,eighteen=3,nineteen=3,twenty=3,twentyone=1,twentyfour=1,twentyfive=2)

    def case7(self):#0,17,18,27,28,36
        t = main_test()
        t.log_in(url_1,user_1,password)
        t.login_client(name=u'lifeng',sex=u'woman',height='168',weight='80',b1='2000',b2='05',b3='04',phone='13717607755',email='lijunfeng@hbldiy.com',diastolic='80',systolic='120',heart='45',gestation='0',lactation='0')
        t.sportandnutritioninadolescence(weight='45',two=3,four=3,six=2,seven=3,eight=3,ten=3,eleven=1,twelve=3,thirteen=1,fourteen=3,sixteen=1,seventeen=3,eighteen=1,nineteen=1,twenty=1,twentyone=3,twentyfour=3,twentyfive=1)
        t.sportandnutritioninadolescence(weight='45',two=2,four=2,six=3,seven=1,eight=2,ten=2,eleven=2,twelve=2,thirteen=2,fourteen=2,sixteen=2,seventeen=2,eighteen=2,nineteen=2,twenty=2,twentyone=1,twentyfour=3,twentyfive=1)
        t.sportandnutritioninadolescence(weight='45',two=2,four=2,six=3,seven=1,eight=2,ten=2,eleven=2,twelve=2,thirteen=2,fourteen=2,sixteen=2,seventeen=2,eighteen=2,nineteen=2,twenty=2,twentyone=1,twentyfour=3,twentyfive=3)
        t.sportandnutritioninadolescence(weight='45',two=1,four=1,six=1,seven=1,eight=1,ten=1,eleven=3,twelve=1,thirteen=3,fourteen=1,sixteen=3,seventeen=1,eighteen=3,nineteen=2,twenty=1,twentyone=3,twentyfour=3,twentyfive=1)
        t.sportandnutritioninadolescence(weight='45',two=1,four=1,six=1,seven=1,eight=1,ten=1,eleven=3,twelve=1,thirteen=3,fourteen=1,sixteen=3,seventeen=1,eighteen=3,nineteen=3,twenty=1,twentyone=3,twentyfour=3,twentyfive=1)
        t.sportandnutritioninadolescence(weight='45',two=1,four=1,six=1,seven=1,eight=1,ten=1,eleven=3,twelve=1,thirteen=3,fourteen=1,sixteen=3,seventeen=1,eighteen=3,nineteen=3,twenty=3,twentyone=1,twentyfour=1,twentyfive=2)



#t = test_case_lactationweek1to6()
#t.case1()


#t = test_case_lactationweek6to12()
#t.case1()

#t = test_case_lactationweek12to16()
#t.case1()

#t = test_case_lactationweek16()
#t.case1()


#t = test_case_lactationweek20to24()
#t.case1()

#t = test_case_lactationweek28to32()
#t.case1()


#t = test_case_sportandnutritioninadolescence()
#t.case3()
